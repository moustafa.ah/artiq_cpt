from artiq.experiment import *
import logging
import numpy as np
from artiq.coredevice.sampler import adc_mu_to_volt

logger = logging.getLogger(__name__)


class Test_asserv_ABR(EnvExperiment):
    '''Test asserv ABR

        Map:
        - Sampler:
            - 7: pd_CPT
        - TTL:
            - 0: pulses_high (high when light is ON)
            - 1: pulses_low (low when light is ON)
        - Urukul:
            - 0: clock_DDS
            - 1: CPT_DDS
        Arguments:
        - t_pump: duration of the bright period after a frequency change
        - t_dark_1: dark period 1 duration
        - t_dark_2: dark period 2 duration
        - t_wait: waiting time after the beginning of a pulse, before data acquisition
        - gain_nu: servo gain for the clock frequency servo integrator
        - gain_ls: servo gain for the light shift servo integrator
        - N_cumul: number of cumulated samples (error) before updating the second order integrator error value
        - N_i2: number of samples to wait before applying the second order integrator correction
        - gain_i2_nu: gain for the second integrator of the clock frequency servo
        - gain_i2_ls: gain for the second integrator of the light shift servo
        - N_update_dataset: number of samples to wait before the frequency is updated in the GUI  
    
    '''
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("clock_DDS")
        self.setattr_device("CPT_DDS")
        self.setattr_device("AOM")
        self.setattr_device("pulses_high")
        self.setattr_device("pulses_low")
    
        self.setattr_argument("t_pump", NumberValue(default=400*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_dark_1", NumberValue(default=200*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_dark_2", NumberValue(default=200*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_wait", NumberValue(default=20*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("gain_nu", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_ls", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_cumul", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_i2", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_i2_nu", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_i2_ls", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_update_dataset", NumberValue(ndecimals=0, step=1))
    
    @kernel
    def init_hardware(self):
        self.core.reset()
        self.sampler0.init()
        self.urukul0_cpld.init()
        self.AOM.cfg_sw(1)  # AOM switched on initially  
        self.core.break_realtime()

    @kernel
    def pulse_1(self, freq_mu, ls_mu, freq_step_mu, update_flag, cumulated_frequency, cumulated_ls):
        """pump pulse
        
        AOM is not switched ON in this pulse but in probe pulse!!!
        
        """
        delay_mu(41040) # Introduce some slack after sampling
                        # Total = 50 us (8.96 us come from delay introduced by sample_mu in pulse_2)
                        # !!! Careful then, since the change in frequency only occurs 80 us after the end of t_wait !!!
        self.CPT_DDS.set_mu(freq_mu+ls_mu+freq_step_mu, pow=0)
        delay_mu(20000) # clock DDS is updated 10 us after CPT DDS for sustained rate purpose
        self.clock_DDS.set_mu(freq_mu, pow=0)
        delay_mu(-22772) # Recover delay introduced by DDS set_mu (2*1386 m.u.), cfg_switch (416 m.u.) and extra 20us in between
        if update_flag == 1:
            update_flag = 0
            # if (self.scheduler.check_pause()):  # causes RTIO underflow. Must kill experiment for now.
            #         update_flag = 2
            self.update_dataset(cumulated_frequency, cumulated_ls)
        delay_mu(-50000) # Remove additional delay introduced at the beginning of the pulse
        delay(self.t_pump)
        return update_flag
    
    @kernel
    def free_evolution(self, freq_mu, freq_step_mu, t_dark):
        self.pulses_high.off()
        self.pulses_low.on()
        self.AOM.cfg_sw(0)
        self.CPT_DDS.set_mu(freq_mu+freq_step_mu, pow=0)
        delay_mu(-1802) # Recover delay introduced by DDS set_mu (1386 m.u.) and cfg_sw (416 m.u.)
        delay(t_dark)

    @kernel
    def pulse_2(self, freq_mu, ls_mu, freq_step_mu, sampler_data):
        """probe pulse

        Returns the CPT value for the DDS frequency defined in pulse_1

        """
        self.pulses_high.on()
        self.pulses_low.off()
        self.CPT_DDS.set_mu(freq_mu+ls_mu+freq_step_mu, pow=0)
        self.AOM.cfg_sw(1)
        delay_mu(-1802) # Recover delay introduced by DDS set_mu (1386 m.u.) and cfg_sw (416 m.u.)
        delay(self.t_wait) 
        self.sampler0.sample_mu(sampler_data)
        return sampler_data[7]

    @kernel
    def cycle(self, freq_mu, ls_mu, list_HWHM_mu, list_t_dark, sl_pattern, lr_pattern, update_flag, cumulated_frequency, cumulated_ls, sampler_data):
        """probe resonance according to patterns
        
        Returns the error signals in a (symmetric) cycle

        """
        error_nu = 0
        error_ls = 0
        for i in range(8):
            sl = sl_pattern[i] # short or long dark period
            sl_idx = (sl+1)//2
            lr= lr_pattern[i] # left or right from resonance
            freq_step_mu = lr*list_HWHM_mu[sl_idx]
            t_dark = list_t_dark[sl_idx]
            update_flag = self.pulse_1(freq_mu, ls_mu, freq_step_mu, update_flag, cumulated_frequency, cumulated_ls)
            if t_dark > 0:
                self.free_evolution(freq_mu, freq_step_mu, list_t_dark[sl_idx]) # avoids updating frequency during free evolution period if t_dark=0. Otherwise, collision occurs.
            pd_cpt_lr = self.pulse_2(freq_mu, ls_mu, freq_step_mu, sampler_data)
            error_nu += lr * pd_cpt_lr
            error_ls += lr * sl * pd_cpt_lr
        return update_flag, error_nu, error_ls

    @rpc(flags={"async"})
    def update_dataset(self, cumulated_frequency, cumulated_ls):
        # logger.info(out_mu)
        self.set_dataset("cumulated_frequency", cumulated_frequency, broadcast=True)
        self.set_dataset("cumulated_ls", cumulated_ls, broadcast=True)
    
    @kernel
    def run(self):
        # Notify the m.u. conversion factors
        logger.info("1 m.u. = %g V", adc_mu_to_volt(1, gain=0)) # We assume there is no gain on Sampler
        logger.info("1 m.u. = %g Hz", self.clock_DDS.ftw_to_frequency(1))
        #
        # Init hardware and create variables 
        self.init_hardware()
        list_HWHM_mu = [self.clock_DDS.frequency_to_ftw(1000. if self.t_dark_1 == 0 else (0.4/self.t_dark_1)),
                        self.clock_DDS.frequency_to_ftw(0.4/self.t_dark_2)] # If t_dark_1=0 (CW), take a modulation amplitude of 1 kHz.
        sampler_data = [0] * 8
        sl_pattern = [-1,-1,1,1,-1,-1,1,1] # ssllssll
        lr_pattern = [-1,1,-1,1,1,-1,1,-1] # lrlrrlrl
        list_t_dark = [self.t_dark_1,self.t_dark_2]
        error_nu = 0
        error_ls = 0
        cumulated_error_nu = 0 # cumulated error in progress 
        cumulated_error_ls = 0 # cumulated error in progress 
        last_cumulated_error_nu = 0 # last recorded cumulated error
        last_cumulated_error_ls = 0 # last recorded cumulated error
        cumulated_frequency = 0 # cumulated frequency for monitoring
        cumulated_ls = 0 # cumulated ls for monitoring
        freq_mu = self.clock_DDS.frequency_to_ftw(10E6) # Initiates frequency to 10 MHz
        ls_mu = 0 # Initiates light shift to 0
        counter_cumul_error = 0
        counter_update_dataset = 0
        counter_i2 = 0 # counter for 2nd order integrator. When =N_i2, updates frequency
        update_flag = 0
        counter_first = 0 # counter that increments for only the first 3 cycles. Break_realtime between these.

        assert self.t_pump > 80*us # t_pump must be at least greater that 80 us
        self.core.break_realtime()      
        #
        # Algorithm
        # process non-frequent events - reset counters
        while update_flag < 2:
            if counter_cumul_error == self.N_cumul:
                last_cumulated_error_nu = cumulated_error_nu
                last_cumulated_error_ls = cumulated_error_ls
                cumulated_error_nu = 0
                cumulated_error_ls = 0
                counter_cumul_error = 0
            if counter_i2 == self.N_i2:
                freq_mu += self.gain_i2_nu * last_cumulated_error_nu # When counter_i2=N_i2, applies the 2nd order integrator correction
                ls_mu += self.gain_i2_ls * last_cumulated_error_ls # When counter_i2=N_i2, applies the 2nd order integrator correction
                counter_i2 = 0
            if counter_update_dataset == self.N_update_dataset:
                update_flag = 1
                cumulated_frequency = 0
                cumulated_ls = 0
                counter_update_dataset = 0
            # process one cycle
            update_flag, error_nu, error_ls = self.cycle(freq_mu, ls_mu, list_HWHM_mu, list_t_dark, sl_pattern, lr_pattern, update_flag, cumulated_frequency, cumulated_ls, sampler_data)
            cumulated_error_nu += error_nu
            cumulated_error_ls += error_ls
            freq_mu += self.gain_nu * error_nu
            ls_mu += self.gain_ls * error_ls
            cumulated_frequency += freq_mu
            cumulated_ls += ls_mu
            # increment counters
            counter_cumul_error +=1
            counter_i2 +=1
            counter_update_dataset+=1
            if counter_first < 3: # For the first 3 cycles, break_realtime
                counter_first+=1
                self.core.break_realtime()