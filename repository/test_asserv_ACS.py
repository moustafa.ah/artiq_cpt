from artiq.experiment import *
import logging
import numpy as np
from artiq.coredevice.sampler import adc_mu_to_volt

logger = logging.getLogger(__name__)


class Test_asserv_ACS(EnvExperiment):
    '''Test asserv ACS

        Map:
        - Sampler:
            - 7: pd_CPT
            - 6: pd_P_incell
        - Urukul:
            - 0: clock_DDS
            - 1: CPT_DDS
            - 2: AOM
        Arguments:
        - t_probe: duration of probing one side of the resonance (half the frequency modulation period)
        - modulation_depth_1: amplitude of the frequency modulation @ P1
        - modulation_depth_2: amplitude of the frequency modulation @ P2
        - p_AOM_1: DDS attenuation (in m.u., from 0 to 255) for AOM when @ P1
        - p_AOM_2: DDS attenuation (in m.u., from 0 to 255) for AOM when @ P2
        - gain_nu: servo gain for the clock frequency servo integrator
        - gain_ls: servo gain for the light shift servo integrator
        - N_cumul: number of cumulated samples (error) before updating the second order integrator error value
        - N_i2: number of samples to wait before applying the second order integrator correction
        - gain_i2_nu: gain for the second integrator of the clock frequency servo
        - gain_i2_ls: gain for the second integrator of the light shift servo
        - N_update_dataset: number of samples to wait before the frequency is updated in the GUI  
    
    '''
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("clock_DDS")
        self.setattr_device("CPT_DDS")
        self.setattr_device("AOM")
        self.setattr_device("pulses_high")
        self.setattr_device("pulses_low")
    
        self.setattr_argument("t_probe", NumberValue(default=400*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("modulation_depth_1", NumberValue(default=2*kHz, ndecimals=6, step=1, unit="Hz"))
        self.setattr_argument("modulation_depth_2", NumberValue(default=2*kHz, ndecimals=6, step=1, unit="Hz"))
        self.setattr_argument("p_AOM_1", NumberValue(default=200, ndecimals=0, step=1))
        self.setattr_argument("p_AOM_2", NumberValue(default=254, ndecimals=0, step=1))
        self.setattr_argument("gain_nu", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_ls", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_cumul", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_i2", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_i2_nu", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("gain_i2_ls", NumberValue(ndecimals=0, step=1))
        self.setattr_argument("N_update_dataset", NumberValue(ndecimals=0, step=1))
    
    @kernel
    def init_hardware(self):
        self.core.reset()
        self.sampler0.init()
        self.urukul0_cpld.init()
        self.AOM.set(110E6, phase=0.0)
        self.AOM.set_att_mu(200)
        self.core.break_realtime()

    @kernel
    def probe(self, freq_mu, ls_mu, freq_step_mu, p_AOM_mu, update_flag, cumulated_frequency, cumulated_ls_coeff, sampler_data):
        """probe one side of the resonance
        
        Returns the CPT transmision and power at cell input values.

        """
        delay_mu(41040) # Introduce some slack after sampling
                        # Total = 80 us (8.96 us come from delay introduced by sample_mu in pulse_2)
                        # !!! Careful then, since the change in frequency only occurs 80 us after the end of t_wait !!!
        self.AOM.set_att_mu(p_AOM_mu)
        self.CPT_DDS.set_mu(freq_mu+ls_mu+freq_step_mu, pow=0)
        delay_mu(20000)
        self.clock_DDS.set_mu(freq_mu, pow=0)    
        delay_mu(-24372) # Recover delay introduced by DDS set_mu (2*1386 m.u.) and set_att_mu (1600 m.u) + extra 20000 m.u.
        if update_flag == 1:
            update_flag = 0
            # if (self.scheduler.check_pause()):  # causes RTIO underflow. Must kill experiment for now.
            #         update_flag = 2
            self.update_dataset(cumulated_frequency, cumulated_ls_coeff)
        delay(self.t_probe - 50*us) # Remove additional delay introduced at the beginning of the pulse
        self.sampler0.sample_mu(sampler_data)
        return update_flag, sampler_data[7], sampler_data[6]
    
    @kernel
    def cycle(self, freq_mu, ls_coeff_mu, list_HWHM_mu, list_p_AOM_mu, list_p_read_mu, p_pattern, lr_pattern, update_flag, cumulated_frequency, cumulated_ls_coeff, sampler_data):
        """probe resonance according to patterns
        
        Returns the error signals in a (symmetric) cycle, and power at cell input.

        """
        error_nu = 0
        error_ls_coeff = 0
        new_list_p_read_mu = [0,0]
        for i in range(8):
            p = p_pattern[i] # p1 or p2 (-1 or 1)
            p_idx = (p+1)//2 # (0 or 1)
            lr= lr_pattern[i] # left or right from resonance (-1 or 1)
            freq_step_mu = lr*list_HWHM_mu[p_idx] # frequency step to apply
            p_read_mu = list_p_read_mu[p_idx] # laser power to be used for ls computation 
            p_AOM_mu = list_p_AOM_mu[p_idx] # attenuation to be sent to AOM
            ls_mu = p_read_mu * ls_coeff_mu # computed light shift
            update_flag, pd_cpt_lr, pd_p_read = self.probe(freq_mu, ls_mu, freq_step_mu, p_AOM_mu, update_flag, cumulated_frequency, cumulated_ls_coeff, sampler_data)
            error_nu += lr * pd_cpt_lr
            error_ls_coeff += lr * p * pd_cpt_lr
            new_list_p_read_mu[p_idx] += pd_p_read # update in cell laser powers by freshly read ones
        return update_flag, error_nu, error_ls_coeff, new_list_p_read_mu

    @rpc(flags={"async"})
    def update_dataset(self, cumulated_frequency, cumulated_ls_coeff):
        # logger.info(out_mu)
        self.set_dataset("cumulated_frequency", cumulated_frequency, broadcast=True)
        self.set_dataset("cumulated_ls_coeff", cumulated_ls_coeff, broadcast=True)
    
    @kernel
    def run(self):
        # Notify the m.u. conversion factors
        logger.info("1 m.u. = %g V", adc_mu_to_volt(1, gain=0)) # We assume there is no gain on Sampler
        logger.info("1 m.u. = %g Hz", self.clock_DDS.ftw_to_frequency(1))
        #
        # Init hardware and create variables 
        self.init_hardware()
        sampler_data = [0] * 8
        p_pattern = [-1,-1,1,1,-1,-1,1,1] # p1 p1 p2 p2 p1 p1 p2 p2
        lr_pattern = [-1,1,-1,1,1,-1,1,-1] # lrlrrlrl
        list_p_AOM_mu = [self.p_AOM_1,self.p_AOM_2]
        list_p_read_mu = [0,0]
        list_HWHM_mu = [self.clock_DDS.frequency_to_ftw(item) for item in [self.modulation_depth_1, self.modulation_depth_2]]
        error_nu = 0
        error_ls_coeff = 0
        cumulated_error_nu = 0 # cumulated error in progress 
        cumulated_error_ls_coeff = 0 # cumulated error in progress 
        last_cumulated_error_nu = 0 # last recorded cumulated error
        last_cumulated_error_ls_coeff = 0 # last recorded cumulated error
        cumulated_frequency = 0 # cumulated frequency for monitoring
        cumulated_ls_coeff = 0 # cumulated ls for monitoring
        freq_mu = self.clock_DDS.frequency_to_ftw(10E6) # Initiates frequency to 10 MHz
        ls_coeff_mu = 0 # Initiates light shift to 0
        counter_cumul_error = 0
        counter_update_dataset = 0
        counter_i2 = 0 # counter for 2nd order integrator. When =N_i2, updates frequency
        update_flag = 0
        counter_first = 0 # counter that increments for only the first 3 cycles. Break_realtime between these.

        assert self.t_probe > 50*us # t_probe must be at least greater that 50 us
        self.core.break_realtime()      
        #
        # Algorithm
        # process non-frequent events - reset counters
        while update_flag < 2:
            if counter_cumul_error == self.N_cumul:
                last_cumulated_error_nu = cumulated_error_nu
                last_cumulated_error_ls_coeff = cumulated_error_ls_coeff
                cumulated_error_nu = 0
                cumulated_error_ls_coeff = 0
                counter_cumul_error = 0
            if counter_i2 == self.N_i2:
                freq_mu += self.gain_i2_nu * last_cumulated_error_nu # When counter_i2=N_i2, applies the 2nd order integrator correction
                ls_coeff_mu += self.gain_i2_ls * last_cumulated_error_ls_coeff # When counter_i2=N_i2, applies the 2nd order integrator correction
                counter_i2 = 0
            if counter_update_dataset == self.N_update_dataset:
                update_flag = 1
                cumulated_frequency = 0
                cumulated_ls_coeff = 0
                counter_update_dataset = 0
            # process one cycle
            update_flag, error_nu, error_ls_coeff, list_p_read_mu = self.cycle(freq_mu, ls_coeff_mu, list_HWHM_mu, list_p_AOM_mu, list_p_read_mu, p_pattern, lr_pattern, update_flag, cumulated_frequency, cumulated_ls_coeff, sampler_data)
            cumulated_error_nu += error_nu
            cumulated_error_ls_coeff += error_ls_coeff
            freq_mu += self.gain_nu * error_nu
            ls_coeff_mu += self.gain_ls * error_ls_coeff
            cumulated_frequency += freq_mu
            cumulated_ls_coeff += ls_coeff_mu
            # increment counters
            counter_cumul_error +=1
            counter_i2 +=1
            counter_update_dataset+=1
            if counter_first < 3: # For the first 3 cycles, break_realtime
                counter_first+=1
                self.core.break_realtime()