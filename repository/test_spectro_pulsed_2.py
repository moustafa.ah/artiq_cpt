from artiq.experiment import *
import logging
import numpy as np
from artiq.coredevice.sampler import adc_mu_to_volt

logger = logging.getLogger(__name__)


class Test_spectro_pulsed_2(EnvExperiment):
    '''Test spectroscopy pulsed - asserv_like

        No dead time as in asserv.
        Change of frequency is done during pump pulse, 50 us after sampling
        Dataset updates are made in the first iteration of pulse_1 at the next probe frequency
        CPT and error signals are cumulated over "N_cumul" iterations
        Map:
        - Sampler:
            - 7: pd_CPT
        - TTL:
            - 0: pulses_high (high when light is ON)
            - 1: pulses_low (low when light is ON)
        - Urukul:
            - 1: CPT_DDS
        Arguments:
        - t_pump: duration 0of the bright period after a frequency change
        - t_dark: dark period duration
        - t_wait: waiting time after the beginning of t2, before data acquisition
        - N_cumul: number of samples cumulated before the frequency is changed
        - is_error_sgnal: if True, also scans an error signal. Else, error signal is 0.
    
    '''
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("CPT_DDS")
        self.setattr_device("AOM")
        self.setattr_device("pulses_high")
        self.setattr_device("pulses_low")
    
        self.setattr_argument("t_pump", NumberValue(default=400*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_dark", NumberValue(default=200*us,ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_wait", NumberValue(default=20*us,ndecimals=6, step=1, unit="s"))
        self.setattr_argument("N_cumul", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument("is_error_signal", BooleanValue(default=False))
        self.setattr_argument(
            "scan", Scannable(
                default=CenterScan(0*kHz, 2*kHz, 0.1*kHz),
                unit="Hz"), tooltip="detuning")        
    
    def init_datasets(self):
        self.set_dataset("detunings", np.asarray(self.scan.sequence), broadcast=True)
        self.set_dataset("cumulated_cpt_signal", np.full(len(self.scan.sequence), np.nan), broadcast=True)
        self.set_dataset("cumulated_error_signal", np.full(len(self.scan.sequence), np.nan), broadcast=True)

    @kernel
    def init_hardware(self):
        self.core.reset()
        self.sampler0.init()
        self.urukul0_cpld.init()
        self.AOM.cfg_sw(1)
        self.core.break_realtime()
        
    @kernel
    def pulse_1(self, freq_mu, freq_step_mu):
        """pump pulse
        
        AOM is not switched ON in this pulse but in probe pulse!!!
        
        """
        delay_mu(41040) # Introduce some slack after sampling
                        # Total = 50 us (8.96 us come from delay introduced by sample_mu in pulse_2)
                        # !!! Careful then, since the change in frequency only occurs 50 us after the end of t_wait !!!
        self.CPT_DDS.set_mu(freq_mu+freq_step_mu, pow=0)
        delay_mu(-1386) # Recover delay introduced by DDS set_mu (1386 m.u.)
        # if (self.scheduler.check_pause()):  # Just wait for the experiment to end or kill it.
        #     terminate_flag = True
        delay_mu(-50000) # Remove additional delay introduced at the beginning of the pulse
        delay(self.t_pump)    
    @kernel
    def free_evolution(self):
        self.AOM.cfg_sw(0)
        delay_mu(-416) # Recover delay introduced by cfg_swself.pulses_high.off()
        self.pulses_low.on()
        delay(self.t_dark)

    @kernel
    def pulse_2(self, sampler_data):
        """probe pulse

        Returns the CPT value for the DDS frequency defined in pulse_1

        """
        self.AOM.cfg_sw(1)
        delay_mu(-416) # Recover delay introduced by cfg_sw
        self.pulses_high.on()
        self.pulses_low.off()
        delay(self.t_wait)
        self.sampler0.sample_mu(sampler_data)
        return sampler_data[7]

    @kernel
    def cycle(self, freq_mu, HWHM_mu, sampler_data):
        """probes at frequency "freq_mu" and returns CPT signal cumulated "N_cumul times".
        
        If error signal is requested, probes also N_cumul_times left then right from resonance.

        """
        # cpt signal
        cpt_signal_value = 0
        for iter in range(self.N_cumul):
            self.pulse_1(freq_mu, 0)
            self.free_evolution()
            cpt_signal_value += self.pulse_2(sampler_data)            
        #error signal
        if self.is_error_signal:
            error_signal_value = 0
            pd_cpt_lr = [0,0]
            for iter in range(self.N_cumul):
                for lr in [-1,1]: # left and right from resonance
                    self.pulse_1(freq_mu, lr*HWHM_mu)
                    self.free_evolution()
                    pd_cpt_lr[(lr+1)//2] += self.pulse_2(sampler_data)
                error_signal_value = pd_cpt_lr[1] - pd_cpt_lr[0]
            return cpt_signal_value, error_signal_value
        return cpt_signal_value, 0
    
    @kernel
    def run(self):
        # TODO: pay attention to the initial conditions on ttls and period of the very first pulse
        # Notify the m.u. to volts conversion factor
        logger.info("1 m.u. = %g V", adc_mu_to_volt(1, gain=0)) # We assume there is no gain on Sampler
        logger.info("1 m.u. = %g Hz", self.CPT_DDS.ftw_to_frequency(1))
        #
        # Init hardware and create variables 
        self.init_hardware()
        # Generate waveforms and init datasets
        self.init_datasets()
        freqs_mu = [(self.CPT_DDS.frequency_to_ftw(elem)) for elem in self.scan.sequence] # converts voltages to words to be sent to the DAC
        sequence_length =len(freqs_mu)
        HWHM_mu = self.CPT_DDS.frequency_to_ftw(0.4/self.t_dark) # We assume FWHM=0.8/t_dark 
        sampler_data = [0] * 8
        list_cpt_signal = [0] * sequence_length
        list_error_signal = [0] * sequence_length
        cpt_signal_value = 0
        error_signal_value = 0
        #
        assert self.t_pump > 50*us # t_dead must be at least greater that 50 us
        self.core.break_realtime()
        self.CPT_DDS.set_mu(freqs_mu[0], pow=0) # set the initial DDS frequency
        delay(1*ms)
        #
        # Algorithm
        for i in range(sequence_length):
            list_cpt_signal[i], list_error_signal[i] = self.cycle(freqs_mu[i], HWHM_mu, sampler_data)
        # Save to datasets
        self.set_dataset("cumulated_cpt_signal", list_cpt_signal, broadcast=True)
        self.set_dataset("cumulated_error_signal", list_error_signal, broadcast=True)