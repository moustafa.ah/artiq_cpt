from artiq.experiment import *
import logging
import numpy as np
from artiq.coredevice.sampler import adc_mu_to_volt

logger = logging.getLogger(__name__)


class Test_spectro_pulsed(EnvExperiment):
    '''Test spectroscopy pulsed, with dead time, clean.

        Change of frequency is done during dead time, 50 us after sampling.
        CPT signal is cumulated over "N_cumul" iterations
        Map:
        - Sampler:
            - 7: pd_CPT
        - TTL:
            - 0: pulses_high (high when light is ON)
            - 1: pulses_low (low when light is ON)
        - Urukul:
            - 1: CPT_DDS
        Arguments:
        - t_pump: duration of the bright period after a frequency change
        - t_dark: dark period duration
        - t_wait: waiting time after the beginning of t2, before data acquisition
        - t_dead: dead time between two cycles
        - N_cumul: number of samples cumulated before the frequency is changed
    
    '''
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("CPT_DDS")
        self.setattr_device("AOM")
        self.setattr_device("pulses_high")
        self.setattr_device("pulses_low")
    
        self.setattr_argument("t_pump", NumberValue(default=400*us, ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_dark", NumberValue(default=200*us,ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_wait", NumberValue(default=20*us,ndecimals=6, step=1, unit="s"))
        self.setattr_argument("t_dead", NumberValue(default=1*ms,ndecimals=6, step=1, unit="s"))
        self.setattr_argument("N_cumul", NumberValue(default=1, ndecimals=0, step=1))
        self.setattr_argument(
            "scan", Scannable(
                default=CenterScan(0*kHz, 2*kHz, 0.1*kHz),
                unit="kHz"), tooltip="detuning")        
    
    def init_datasets(self):
        self.set_dataset("detunings", np.asarray(self.scan.sequence), broadcast=True)
        self.set_dataset("cumulated_cpt_signal", np.full(len(self.scan.sequence), np.nan), broadcast=True)

    @kernel
    def init_hardware(self):
        self.core.reset()
        self.sampler0.init()
        self.urukul0_cpld.init()
        self.AOM.cfg_sw(1)
        self.core.break_realtime()
        
    @kernel
    def pulse_1(self):
        """pump pulse"""
        self.AOM.cfg_sw(1)
        delay_mu(-416) # Recover delay introduced by cfg_sw
        self.pulses_high.on()
        self.pulses_low.off()
        delay(self.t_pump)
    
    @kernel
    def free_evolution(self):
        self.AOM.cfg_sw(0)
        delay_mu(-416) # Recover delay introduced by cfg_sw
        self.pulses_high.off()
        self.pulses_low.on()
        delay(self.t_dark)

    @kernel
    def pulse_2(self, sampler_data):
        """probe pulse

        Returns the CPT value for the DDS frequency defined in last "dead_time"

        """
        self.AOM.cfg_sw(1)
        delay_mu(-416) # Recover delay introduced by cfg_sw
        self.pulses_high.on()
        self.pulses_low.off()
        delay(self.t_wait)
        self.sampler0.sample_mu(sampler_data)
        return sampler_data[7]

    @kernel
    def dead_time(self, freq_mu, freq_step_mu, ask_terminate, terminate_flag, update_flag, cpt_signal_value, i):
        """dead time

        Frequency change is done here when last iteration of "N_cumul".
        Mutate_dataset is done here as well.
        Check_pause also is done here, but shunt if "t_dead" < 10 ms.
        
        """
        delay_mu(41040) # Introduce some slack after sampling
                        # Total = 50 us after sampling (8.96 us come from delay introduced by sample_mu in pulse_2)
                        # !!! Careful then, since the change in frequency only occurs 50 us after the end of t_wait !!!
        self.AOM.cfg_sw(0)
        self.pulses_high.off()
        self.pulses_low.on()
        self.CPT_DDS.set_mu(freq_mu+freq_step_mu, pow=0)
        delay_mu(-1802) # Recover delay introduced by DDS set_mu (1386 m.u.) and cfg_sw (461 m.u.)
        if update_flag:
            self.update_dataset(cpt_signal_value, i)
        if ask_terminate :
            if (self.scheduler.check_pause()):  # causes RTIO underflow. Must kill experiment for now.
                terminate_flag = True
        delay_mu(-50000) # Remove additional delay introduced at the beginning of the pulse
        delay(self.t_dead)
        return terminate_flag

    @kernel
    def cycle(self, freq_mu, next_freq_mu, HWHM_mu, ask_terminate, terminate_flag, sampler_data, i):
        """probes at frequency "freq_mu" and returns CPT signal cumulated "N_cumul" times"""
        cpt_signal_value = 0
        for iter in range(self.N_cumul):
            self.pulse_1()
            self.free_evolution()
            cpt_signal_value += self.pulse_2(sampler_data)
            if iter == self.N_cumul-1:
                terminate_flag = self.dead_time(next_freq_mu, 0, ask_terminate, terminate_flag, True, cpt_signal_value, i)
            else:
                terminate_flag = self.dead_time(freq_mu, 0, ask_terminate, terminate_flag, False, cpt_signal_value, i)
            if terminate_flag :
                return terminate_flag
        return False

    @rpc(flags={"async"})
    def update_dataset(self, cumulated_CPT_signal, i):
        """ updates cumulated CPT signal dataset """
        self.mutate_dataset("cumulated_cpt_signal", i, cumulated_CPT_signal)

    @kernel
    def run(self):
        # TODO: pay attention to the initial conditions on ttls and period of the very first pulse
        # Notify the m.u. to volts conversion factor
        logger.info("1 m.u. = %g V", adc_mu_to_volt(1, gain=0)) # We assume there is no gain on Sampler
        logger.info("1 m.u. = %g Hz", self.CPT_DDS.ftw_to_frequency(1))
        #
        # Init hardware 
        self.init_hardware()
        #
        # Generate waveforms and init datasets
        self.init_datasets()
        freqs_mu = [(self.CPT_DDS.frequency_to_ftw(elem)) for elem in self.scan.sequence] # converts voltages to words to be sent to the DAC
        sequence_length =len(freqs_mu)
        HWHM_mu = self.CPT_DDS.frequency_to_ftw(0.4/self.t_dark) # We assume FWHM=0.8/t_dark. Not used in this experiment
        sampler_data = [0] * 8
        terminate_flag = False

        assert self.t_dead > 200*us # t_dead must be at least greater that 50 us
        ask_terminate = False
        if self.t_dead >= 10*ms:
            ask_terminate = True # shunt check_pause if t_dead is small
        self.core.break_realtime()
        self.CPT_DDS.set_mu(freqs_mu[0], pow=0) # set the initial DDS frequency
        delay(1*ms)
        #
        # Algorithm
        for i in range(sequence_length):
            if i == sequence_length-1:
                i = -1 # at the end of the sequence, bring back the DDS frequency to the initial one
            if terminate_flag:
                break
            terminate_flag = self.cycle(freqs_mu[i], freqs_mu[i+1], HWHM_mu, ask_terminate, terminate_flag, sampler_data, i)