from artiq.experiment import *
import logging
import numpy as np
from artiq.coredevice.sampler import adc_mu_to_volt

logger = logging.getLogger(__name__)



class Test_spectro_CW(EnvExperiment):
    '''Test spectroscopy CW
    
    Map:
        - Sampler:
            - 7: pd_CPT
        # - TTL:
        #     - 0: pulses_high (high when light is ON)
        #     - 1: pulses_low (low when light is ON)
        - Urukul:
            - 1: CPT_DDS
        Arguments:
        - t_probe: duration of probing one side of the resonance (half the frequency modulation period)
        - modulation_depth: amplitude of the frequency modulation
        - N_cumul: number of cumulated samples (error) before updating the second order integrator error value
        - is_error_sgnal: if True, also scans an error signal. Else, error signal is 0.  
    
    '''
    
    def build(self):
        self.setattr_device("core")
        self.setattr_device("scheduler")
        self.setattr_device("sampler0")
        self.setattr_device("urukul0_cpld")
        self.setattr_device("CPT_DDS")
        
        self.setattr_argument("t_probe", NumberValue(ndecimals=1, step=1, unit="us"))
        self.setattr_argument("modulation_depth", NumberValue(default=2*kHz, ndecimals=6, step=1, unit="Hz"))
        self.setattr_argument("N_cumul", NumberValue(ndecimals=0, step=1, scale = 1))
        self.setattr_argument("is_error_signal", BooleanValue(default=False))
        self.setattr_argument(
            "scan", Scannable(
                default=CenterScan(0.*kHz, 2.*kHz, 0.1*kHz),
                unit="kHz"), tooltip="detuning")
        
    def init_datasets(self):
        self.set_dataset("detunings", np.asarray(self.scan.sequence), broadcast=True)
        self.set_dataset("cumulated_cpt_signal", np.full(len(self.scan.sequence), np.nan), broadcast=True)
        self.set_dataset("cumulated_error_signal", np.full(len(self.scan.sequence), np.nan), broadcast=True)

    @kernel
    def init_hardware(self):
        self.core.reset()
        self.sampler0.init()
        self.urukul0_cpld.init()
        self.core.break_realtime()
    
    @kernel
    def probe(self, freq_mu, freq_step_mu, sampler_data):
        """probe one side of the resonance"""
        delay_mu(21040) # Introduce some slack after sampling
                        # Total = 30 us (8.96 us come from delay introduced by sample_mu in pulse_2)
                        # !!! Careful then, since the change in frequency only occurs 50 us after the end of t_wait !!!
        self.CPT_DDS.set_mu(freq_mu+freq_step_mu, pow=0)
        delay_mu(-1386) # Recover delay introduced by DDS set_mu (1386 m.u.)            
        delay_mu(-30000) # Remove additional delay introduced at the beginning of the pulse
        delay(self.t_probe)
        self.sampler0.sample_mu(sampler_data)
        return sampler_data[7]

    @kernel
    def cycle(self, freq_mu, HWHM_mu, sampler_data):
        """probes at frequency "freq_mu" and returns CPT signal cumulated "N_cumul times".
        
        If error signal is requested, probes also N_cumul_times left then right from resonance.

        """
        # cpt signal
        cpt_signal_value = 0
        for iter in range(self.N_cumul):
            cpt_signal_value += self.probe(freq_mu, 0, sampler_data)
            
        #error signal
        if self.is_error_signal:
            error_signal_value = 0
            pd_cpt_lr = [0,0]
            for iter in range(self.N_cumul):
                for lr in [-1,1]: # left and right from resonance
                    pd_cpt_lr[(lr+1)//2] += self.probe(freq_mu, lr*HWHM_mu, sampler_data)
                error_signal_value = pd_cpt_lr[1] - pd_cpt_lr[0]
            return cpt_signal_value, error_signal_value
        return cpt_signal_value, 0
        
    @kernel
    def run(self):
        # Notify the m.u. to volts conversion factor
        logger.info("1 m.u. = %g V", adc_mu_to_volt(1, gain=0)) # We assume there is no gain on Sampler
        logger.info("1 m.u. = %g Hz", self.CPT_DDS.ftw_to_frequency(1))
        #
        # Init hardware and create variables 
        self.init_hardware()
        # Generate waveforms and init datasets
        self.init_datasets()
        freqs_mu = [(self.CPT_DDS.frequency_to_ftw(elem)) for elem in self.scan.sequence] # converts voltages to words to be sent to the DAC
        sequence_length =len(freqs_mu)
        HWHM_mu = self.CPT_DDS.frequency_to_ftw(self.modulation_depth)
        sampler_data = [0] * 8
        list_cpt_signal = [0] * sequence_length
        list_error_signal = [0] * sequence_length
        cpt_signal_value = 0
        error_signal_value = 0
        
        assert self.t_probe > 30*us # t_dead must be at least greater that 50 us
        self.core.break_realtime()
        
        #
        # Algorithm
        while not (self.scheduler.check_pause()):
            self.core.break_realtime()
            self.CPT_DDS.set_mu(freqs_mu[0], pow=0) # set the initial DDS frequency
            for i in range(sequence_length):
                list_cpt_signal[i], list_error_signal[i] = self.cycle(freqs_mu[i], HWHM_mu, sampler_data)
            # Save to datasets
            self.set_dataset("cumulated_cpt_signal", list_cpt_signal, broadcast=True)
            self.set_dataset("cumulated_error_signal", list_error_signal, broadcast=True)